Bootstrap: docker
From: ubuntu:16.04

%help
This is OpenFace. Install script adapted from https://github.com/TadasBaltrusaitis/OpenFace/wiki/Unix-Installation

# =======================
# global
# =======================
%post
    apt-get -y update
    apt-get -y install build-essential cmake libopenblas-dev wget unzip
    apt-get -y install git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
    apt-get -y install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libdc1394-22-dev
    apt-get -y install libboost-all-dev

    wget https://github.com/opencv/opencv/archive/3.4.0.zip
    unzip 3.4.0.zip
    rm 3.4.0.zip
    cd opencv-3.4.0
    mkdir build
    cd build
    cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_SHARED_LIBS=OFF ..
    make -j2
    make install
    cd

    wget http://dlib.net/files/dlib-19.13.tar.bz2
    tar xf dlib-19.13.tar.bz2
    rm dlib-19.13.tar.bz2
    cd dlib-19.13
    mkdir build
    cd build
    cmake ..
    cmake --build . --config Release
    make install 
    ldconfig
    cd

    git clone --branch OpenFace_2.0.6 https://github.com/TadasBaltrusaitis/OpenFace.git
    cd OpenFace
    rm -rf .git*
    mkdir build
    cd build
    cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D CMAKE_CXX_FLAGS="-std=c++11" -D CMAKE_EXE_LINKER_FLAGS="-std=c++11" ..
    make
    make install


%environment
export LC_ALL=C


%apprun FaceLandmarkImg
exec /usr/local/bin/FaceLandmarkImg "$@"

%apprun FaceLandmarkVid
exec /usr/local/bin/FaceLandmarkVid "$@"

%apprun FaceLandmarkVidMulti
exec /usr/local/bin/FaceLandmarkVidMulti "$@"

%apprun FeatureExtraction
exec /usr/local/bin/FeatureExtraction "$@"


